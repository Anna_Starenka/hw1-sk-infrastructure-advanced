const gulp = require('gulp');
const htmlMin = require('gulp-htmlmin')

const concat = require('gulp-concat')


const cleanCSS = require('gulp-clean-css');

const clean = require('gulp-clean')

const browerSync = require('browser-sync').create();


const sass = require('gulp-sass')(require('sass'));


const html = ()=>{
    return gulp.src('./src/*.html')
    .pipe(htmlMin({ collapseWhitespace: true }))
    .pipe(gulp.dest('./dist'))
}


const css = () => {
    return gulp.src('./src/styles/**/*.css')
        .pipe(concat('style.css'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./dist/styles'))
}


const scss = () => {
    return gulp.src('./src/style/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./dist/styles'))
};



const image =()=>{
    return gulp.src('./src/images/**/*.*')
    .pipe(gulp.dest('./dist/images'))
}
const server =()=>{
    browerSync.init({
        server:{
            baseDir: './dist'
        }
    })
}


const watcher =()=>{
    gulp.watch('./src/**/*.html', html).on('all', browerSync.reload)
    gulp.watch('./src/styles/**/*.{scss, sass, css}', scss).on('all', browerSync.reload)
}

gulp.task('html', html);
gulp.task('style', css);
gulp.task('scss', scss)
gulp.task('browser-synk', server)


const cleanDist =()=>{
    return gulp.src('./dist', { read:false }).pipe(clean())
}

gulp.task('build', gulp.series(
    cleanDist,
    gulp.parallel(html, scss, image)
    ));



gulp.task('dev', gulp.series(
    gulp.parallel(html, scss, image),
    gulp.parallel(server, watcher)
));
